package myArrayList.util;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Results implements FileDisplayInterface, StdoutDisplayInterface {
	String str;

	private String[] data = new String[100];
	int i = 1;
	FileWriter fileWriter;
	private int counter = 0;
	PrintWriter printWriter;
	String file = str;
	int j = 0;

	@Override
	public void writeToFile() {
		try {
			printWriter = new PrintWriter(new FileWriter(file));
			for (int i = 0; i < j; i++) {
				if (i == 4) {
					printWriter.println("\nTEST CASES STARTS HERE\n");
				}
				printWriter.println(data[i]);
			}
		} catch (IOException e) {
			System.out.println("Please enter the file to print !");
		}
		printWriter.close();

	}

	@Override
	public void writeToStdout(String s) {
		if (counter == 4) {
			System.out.println("\nTEST CASES STARTS HERE\n");
		}
		System.out.println(s);
		counter++;
	}

	public void generalmethod(String s) {
		data[j] = s;
		j++;
	}

	public void storeNewResult(String test, boolean result) {
		if (result == true) {

			String s = test + " PASSED";
			data[j] = s;
			this.writeToStdout(s);
			j++;
		} else {
			String s = test + " FAILED";
			data[j] = s;
			this.writeToStdout(s);
			j++;
		}
	}

	public void recevieoutputfile(String s) {
		str = s;
		file = str;
	}
}
