package myArrayList.test;

import java.util.Arrays;
import java.util.Random;

import myArrayList.MyArrayList;
import myArrayList.util.Results;

public class MyArrayListTest {

	public void testMe(MyArrayList myArrayList, Results results) {
		boolean st1;
		myArrayList.empty();
		st1 = this.test1(myArrayList);
		results.storeNewResult("Test Inserts an element and calculate sum of array values", st1);
		myArrayList.empty();

		st1 = this.test2(myArrayList);
		results.storeNewResult("Test Compares the size of array and Sum of array", st1);
		myArrayList.empty();

		st1 = this.test3(myArrayList);
		results.storeNewResult("Test Inserts 3 elements in different arrays compare the results", st1);
		myArrayList.empty();

		st1 = this.test4(myArrayList);
		results.storeNewResult("Test Return index of duplicate value inserted", st1);
		myArrayList.empty();

		st1 = this.test5(myArrayList);
		results.storeNewResult("Test inserted values and then remove a selected value and compare the sums", st1);
		myArrayList.empty();

		st1 = this.test6(myArrayList);
		results.storeNewResult("Test Insert elements, delete elements and check the sum of array", st1);
		myArrayList.empty();

		st1 = this.test7(myArrayList);
		results.storeNewResult("Test Insert elements , calculate sum of insertions index", st1);
		myArrayList.empty();

		st1 = this.test8(myArrayList);
		results.storeNewResult("Test element exceeding boundary", st1);
		myArrayList.empty();

		st1 = this.test9(myArrayList);
		results.storeNewResult("Test Removing an element from empty list", st1);
		myArrayList.empty();

		st1 = this.test10(myArrayList);
		results.storeNewResult("Test Size of empty list", st1);
		myArrayList.empty();

	}

	public boolean test1(MyArrayList list) {
		list.countreset();
		list.insertSorted(5);
		boolean testResult;
		int sum = list.calculateSum();
		if (sum == 5) {
			testResult = true;
		} else {
			testResult = false;
		}
		return testResult;
	}

	public boolean test2(MyArrayList list) {

		boolean testResult = false;
		list.countreset();
		for (int i = 0; i < 10; i++) {
			list.insertSorted(1);
		}
		int sum = list.calculateSum();
		// System.out.println(sum);
		int size = list.size();
		// System.out.println(size);
		if (sum == size) {
			testResult = true;
		} else {
			testResult = false;
		}
		// System.out.println(testResult);
		return testResult;

	}

	public boolean test3(MyArrayList list) {

		// this test case inserts 3 elements in array in compares if the arrays
		// are
		// equal
		int k = 1;
		int[] array1 = new int[3];
		for (int j = 0; j < 3; j++) {
			array1[j] = k;
			k++;
		}
		list.countreset();
		list.insertSorted(1);
		list.insertSorted(2);
		list.insertSorted(3);

		boolean s = list.compareArrays(array1);
		return s;

	}

	public boolean test4(MyArrayList list) {

		// return index of duplicate value inserted
		boolean testResult = false;
		String expectedResult = "1";
		list.countreset();
		list.insertSorted(0);
		list.insertSorted(1);
		list.insertSorted(2);
		list.insertSorted(3);
		int k = Integer.parseInt(expectedResult);

		list.insertSorted(k);
		String result = "" + list.indexOf(1);
		testResult = list.compareValues(result, expectedResult);
		// System.out.println("result is :" + result);

		return testResult;

	}

	public boolean test5(MyArrayList list) {

		// calculate sum of inserted values and then remove the value on fourth
		// index and calulate it again
		boolean testResult = false;
		String expectedResult = "15";
		list.countreset();
		list.insertSorted(0);
		list.insertSorted(1);
		list.insertSorted(2);
		list.insertSorted(3);
		list.insertSorted(4);
		list.insertSorted(5);
		String sum = "" + list.calculateSum();
		list.removeValue(4);
		String result = "" + list.calculateSum();
		testResult = list.compareValues(result, expectedResult);

		return testResult;

	}

	public boolean test6(MyArrayList list) {

		// insert element , delete same element and checking sum of array
		// elements
		boolean testResult = false;
		list.countreset();
		String expectedResult = "0";
		list.insertSorted(0);
		list.removeValue(0);
		list.insertSorted(1);
		list.removeValue(1);
		list.insertSorted(2);
		list.removeValue(2);
		list.insertSorted(3);
		list.removeValue(3);
		list.insertSorted(4);
		list.removeValue(4);

		String res = "" + list.calculateSum();
		testResult = list.compareValues(res, expectedResult);

		// System.out.println("Sum is " + list.calculateSum());
		return testResult;

	}

	public boolean test7(MyArrayList list) {

		// insert element , sum the index values and
		// elements
		boolean testResult = false;
		String expectedResult;
		list.countreset();
		list.insertSorted(0);
		list.insertSorted(1);
		list.insertSorted(2);
		list.insertSorted(3);
		int add = 0;
		for (int i = 0; i < 4; i++) {
			add += list.indexOf(i);
		}

		// System.out.println("Val is " + add);
		expectedResult = "" + add;

		String res = "" + list.calculateSum();
		testResult = list.compareValues(res, expectedResult);

		// System.out.println("Sum is " + list.calculateSum());
		return testResult;

	}

	public boolean test8(MyArrayList list) {

		// insert element exceeding boundry
		int i;
		list.countreset();
		boolean testResult = true;
		for (i = 0; i < 50; i++) {
			int k = (int) (Math.random() * 10);
			list.insertSorted(k);
		}

		for (i = 50; i < 100; i++) {
			int k = (int) (Math.random() * 10);
			list.insertSorted(k);
		}

		return testResult;

	}

	public boolean test9(MyArrayList list) {

		// Removing an element from empty list
		// int i;
		// String result = "4";
		boolean testResult = false;
		list.countreset();
		list.removeValue(5);
		return testResult;
	}

	public boolean test10(MyArrayList list) {

		// Size of list
		// int i;
		String result = "4";
		list.countreset();
		boolean testResult = false;
		String k = "" + list.size();
		testResult = list.compareValues(k, result);

		// System.out.println("List is empty");
		return testResult;
	}

}
