package myArrayList.driver;

import myArrayList.MyArrayList;
import myArrayList.test.MyArrayListTest;
import myArrayList.util.FileProcessor;
import myArrayList.util.Results;

public class Driver {

	public static void main(String[] args) {

		Results res = new Results();
		String inputFile = null;

		try {
			inputFile = args[0];
			if (!(inputFile.equals("input.txt"))) {
				System.out.println("No Input File Present Please Enter It");
				System.exit(0);
			}
		} catch (Exception e) {
			System.out.println("No Input File Present Please Enter It");
			System.exit(0);
		}
		String outputFile = null;
		try {
			outputFile = args[1];
			if (!(outputFile.equals("output.txt"))) {
				System.out.println("No Ouput File Present Please Enter It");
				System.exit(0);
			}

		} catch (Exception e) {
			System.out.println("No Output File Present Please Enter It");
			System.exit(0);
		}

		res.recevieoutputfile(outputFile);
		String str = null;
		MyArrayList my = new MyArrayList();
		FileProcessor fp = new FileProcessor();
		while ((str = fp.ReadLine(inputFile)) != null)
			my.insertSorted(Integer.parseInt(str));
		my.removeValue(5);

		System.out.println("The elements in the array after inserted from a file are : " + my.toString());
		String s1 = "The elements in the array after inserted from a file are : " + my.toString();
		res.generalmethod(s1);
		String line = "The sum of all the values in the array list is: ";
		int sum = my.calculateSum();
		int size = my.size();
		String s = "Number of elements in the array are :" + size;
		res.generalmethod(s);
		res.writeToStdout(s);
		int l = my.indexOf(23);
		String index = "Index of searched element :" + l;
		res.generalmethod(index);
		res.writeToStdout(index);

		line = line + sum;
		res.writeToStdout(line);
		res.generalmethod(line);
		MyArrayListTest test = new MyArrayListTest();
		test.testMe(my, res);

		res.writeToFile();
	}

}
