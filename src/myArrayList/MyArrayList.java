package myArrayList;

import java.util.Arrays;

import myArrayList.util.Results;

public class MyArrayList {
	private int[] array;
	int count = 0;
	int counter = 0;
	Results res = new Results();
	int arrsize = 50;
	int s = 0;
	int globalcounter = 0;

	public MyArrayList() {
		array = new int[arrsize];

	}

	public void countreset() {
		count = 0;
	}

	public void insertSorted(int newValue) {
		globalcounter++;

		if (count == 0) {
			array[0] = newValue;
			count = count + 1;
		} else {
			int s = count + 1;
			if (arrsize <= s) {
				int temp_array[];
				temp_array = Arrays.copyOf(array, array.length);
				arrsize = arrsize + 25;
				array = new int[arrsize];

				int t = 0;
				while (t < count) {
					array[t] = temp_array[t];
					t++;
				}
			}
			int i;
			for (i = 0; i < count; i++) {
				if (newValue < array[i]) {
					break;
				}
			}

			for (int j = count; j >= i; j--) {
				int s1 = j + 1;
				array[s1] = array[j];
			}
			array[i] = newValue;
			int l = count + 1;
			count = l;
		}
	}

	public void empty() {
		for (int i = 0; i < array.length; i++) {
			array[i] = 0;
		}
	}

	public boolean compareArrays(int arr[]) {
		boolean k = false;
		int flag = 0;
		int i = 0;
		for (i = 0; i < arr.length; i++) {
			// System.out.println("i is " + i);
			if (array[i] == arr[i]) {
				// System.out.println("Came here");
				flag++;
			}
		}
		if (flag == arr.length) {
			k = true;
		}
		return k;
	}

	public int indexOf(int value) {
		int i;
		int flag = 0;
		for (i = 0; i < array.length; i++) {
			if (array[i] == value) {
				flag = 1;
				return i;
			}
		}
		if (flag == 1) {

		} else {
			i = -1;
		}

		return i;

	}

	public int size() {
		int k = count;
		return k;
	}

	public void removeValue(int value) {

		int[] filtered = Arrays.stream(array).filter(i -> i != value).toArray();
		int temp_array[];
		temp_array = Arrays.copyOf(filtered, filtered.length);

		int t = 0;
		while (t < filtered.length) {
			array[t] = temp_array[t];
			t++;
		}

		calculateSum();
	}

	public int calculateSum() {
		int sum = 0;
		for (int i = 0; i < array.length; i++) {
			sum = sum + array[i];
		}
		return sum;
	}

	public boolean compareValues(String result, String expectedResult) {

		boolean flag;
		if (expectedResult.equals(result)) {
			flag = true;
		} else {
			flag = false;
		}
		return flag;
	}

	@Override
	public String toString() {
		// System.out.println("MyArrayList :" + Arrays.toString(array));
		return Arrays.toString(array);
	}

}
